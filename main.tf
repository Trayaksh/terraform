provider "aws" {
  region = "us-east-2"
  access_key = "AKIARAG6YPXLJ7AXKRQH"
  secret_key = "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"
}

variable "cidr_blocks" {
  description = "cidr block for vpc and subnet"
  type = list(string)  
}

# variable "vpc_cidr_block" {
#   description = "vpc cidr block"
# }

# variable "vpc_environment" {
#   description = "develop"
# }

resource "aws_vpc" "dev-vpc" {
  cidr_block = var.cidr_blocks[0]
  tags = {
    Name: "develop"
    vpc_env: "dev"
  }
  }

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.dev-vpc.id
  cidr_block = var.cidr_blocks[1]
  availability_zone = "us-east-2a"
  tags = {
    Name: "subnet-1-dev"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.dev-vpc.id
}

output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}